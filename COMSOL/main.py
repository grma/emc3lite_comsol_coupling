import csv
import numpy as np
import kisslinger2comsol as k2c
import target_particle_flux as flux
from os.path import basename

"""
Created on 19.09.2024

@author: grma
"""

k2c.kisslinger2comsol("Gap_Closure/cover_lower_chamber_gap")
k2c.kisslinger2comsol("Gap_Closure/cover_lower_gap_0-1.0")
k2c.kisslinger2comsol("Gap_Closure/cover_upper_gap_0_1.5")
k2c.kisslinger2comsol("Gap_Closure/cover_lower_gap_12_26")
k2c.kisslinger2comsol("Gap_Closure/cover_upper_chamber_gap")




reader = flux.read_TARGET_PROFILES()
#reader.print_arrays()
reader.get_points()
