# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 11:26:03 2022

@author: ygao
"""
import csv
import numpy as np
from os.path import basename

class read_TARGET_PROFILES:
    def __init__(self):
        file = open("../PROCESS/OUTPUT/TARGET_PROFILES", "r")
        bb = file.readlines()
        file.close()
        firstrow = np.array(bb[0].split(' '))
        firstrow = firstrow[firstrow != ''].astype(int)
        self.total_elements = int(firstrow[0])
        self.total_components = int(firstrow[1])
        
        NOPFCs, PFCnames, power_Ws, N_Pols, N_Tols, data_st_rows = [], [], [], [], [], []
        for row in range(len(bb)):
            if './WALL_DIV' in bb[row]:
                wdname = np.array(bb[row].split(' '))
                wdname = wdname[wdname != '']
                NOPFCs.append(int(wdname[0]))
                PFCnames.append(basename(wdname[1]))
                power_Ws.append(float(bb[row + 1].split('! power')[0]))
                dimen = np.array(bb[row + 2].split(' '))
                dimen = dimen[dimen != '']
                N_Pols.append(int(dimen[0]))
                N_Tols.append(int(dimen[1]))
                data_st_rows.append(row + 3)
        
        self.NOPFCs, self.PFCnames, self.power_Ws, self.N_Pols, self.N_Tols, self.data_st_rows = \
            np.array(NOPFCs), np.array(PFCnames), np.array(power_Ws), np.array(N_Pols), np.array(N_Tols), np.array(data_st_rows)
        
        # get data now
        self.Rcs, self.Zcs, self.phics, self.datas = [], [], [], []
        self.Rs, self.Zs, self.phis = [], [], []
        for i in range(len(self.data_st_rows)):
            sindex = self.data_st_rows[i]
            if i == (len(self.data_st_rows) - 1):
                eindex = len(bb)
            else:
                eindex = self.data_st_rows[i+1] - 3
            stringtemp = bb[sindex:eindex]
            
            arrayall = []
            for str1 in stringtemp:
                arrayall.extend([float(x) for x in str1.split()])
            arrayall = np.array(arrayall)
     
            grid_size = (self.N_Pols[i] + 1) * (self.N_Tols[i] + 1)
            # data is in the center of each grid (rectangular) boundary
            data_size = self.N_Pols[i] * self.N_Tols[i]
            if len(arrayall) != 3 * grid_size + data_size:
                print ('Data_format is wrong for some reason')
            R = arrayall[:(grid_size)].reshape(self.N_Tols[i] + 1, self.N_Pols[i] + 1)
            Z = arrayall[grid_size : (2 * grid_size)].reshape(self.N_Tols[i] + 1, self.N_Pols[i] + 1)
            phi = arrayall[(2 * grid_size) : (3 * grid_size)].reshape(self.N_Tols[i] + 1, self.N_Pols[i] + 1)
            data = arrayall[(3 * grid_size) : (3 * grid_size + data_size)].reshape(self.N_Tols[i], self.N_Pols[i])
            
            # now to get the center of the grid
            # firstly get the row
            Rc = np.diff(R, axis = 0) / 2 + R[:(self.N_Tols[i])]
            Zc = np.diff(Z, axis = 0) / 2 + Z[:(self.N_Tols[i])]
            phic = np.diff(phi, axis = 0) / 2 + phi[:(self.N_Tols[i])]
            # secondly get the column
            Rc = np.diff(Rc, axis = 1) / 2 + Rc[:,:(self.N_Pols[i])]
            Zc = np.diff(Zc, axis = 1) / 2 + Zc[:,:(self.N_Pols[i])]
            phic = np.diff(phic, axis = 1) / 2 + phic[:,:(self.N_Pols[i])]
            
            self.Rs.append(R)
            self.Zs.append(Z)
            self.phis.append(phi)
            
            self.Rcs.append(Rc)
            self.Zcs.append(Zc)
            self.phics.append(phic)
            self.datas.append(data)
            
        self.Rs, self.Zs, self.phis = np.array(self.Rs, dtype=object), np.array(self.Zs, dtype=object), \
            np.array(self.phis, dtype=object)
        self.Rcs, self.Zcs, self.phics, self.datas = np.array(self.Rcs, dtype=object), np.array(self.Zcs, dtype=object), \
            np.array(self.phics, dtype=object), np.array(self.datas, dtype=object)

    """
    Created on 17.09.2024

    @author: grma
    """
    
    def print_arrays(self):
        #print("NOPFCs:", self.NOPFCs)
        print("PFCnames:", self.PFCnames)
        print("power_Ws:", self.power_Ws)
        print("N_Pols:", self.N_Pols)
        print("N_Tols:", self.N_Tols)
        #print("data_st_rows:", self.data_st_rows)
        #print(len(self.phis[5][0]))
        #print("Zs:", self.Zs)
        #print("phis:", self.phis)
        #print("Rcs:", self.Rcs[0][1][123])
        #print("Rcs:", len(self.Rcs[0]))
        #print("Zcs:", self.Zcs)
        #print(len(self.phis[5][0]))
        #print(len(self.phics[5][0]))
        #print(max(self.datas[5][75]))
        #print("datas:", self.datas)
    def check_data(self):        
        for i in range(len(self.PFCnames)):
            test = 0
            max = 0.0
            for k in range(self.N_Tols[i]):
                for l in range(self.N_Pols[i]):
                    value = self.datas[i][k][l]
                    if (max < value):
                        max = value
                    test += value
            #print(test)
            print(max)
    def get_points(self):
        for i in range(len(self.PFCnames)):
            #grid sizes
            tor_cuts = len(self.Rs[i])
            pol_cuts = len(self.Rs[i][0])
            #print(tor_cuts)
            #print(pol_cuts)
            point_count = tor_cuts * pol_cuts 
            element_count = (tor_cuts - 1) * (pol_cuts - 1)
            #buffer array to store deposition per node (the original is stored on the element)
            node_data = np.zeros((tor_cuts, pol_cuts))
            #increment array to store the number of accumulated element data
            count = np.zeros((tor_cuts, pol_cuts))
           
            for k in range(tor_cuts - 1):
                for l in range(pol_cuts - 1):
                    #add deposition per element to the surrounding nodes
                    node_data[k, l] += self.datas[i][k][l]
                    node_data[k + 1, l] += self.datas[i][k][l]
                    node_data[k, l + 1] += self.datas[i][k][l]
                    node_data[k + 1, l + 1] += self.datas[i][k][l]
                    #add increment to surrounding nodes
                    count[k, l] += 1
                    count[k + 1, l] += 1
                    count[k, l + 1] += 1
                    count[k + 1, l + 1] += 1
                    
            #averaged deposition per node
            node_data /= count
            #print(node_data[0])
           
            #write all values in one array
            coordinates_buffer = np.zeros((point_count, 3))
            Rs_buffer = []
            Zs_buffer = []
            phis_buffer = []     
            datas_buffer = [] 
              
            Xs_buffer = []
            Ys_buffer = []   
            for k in range(tor_cuts):
                for l in range(pol_cuts):
                    
                    Rs_buffer.append(self.Rs[i][k][l])
                    Zs_buffer.append(self.Zs[i][k][l])
                    phis_buffer.append(self.phis[i][k][l])
                    datas_buffer.append(node_data[k][l])
            #convert to cartesian coordinates
            Xs_buffer = Rs_buffer * np.cos(phis_buffer)
            Ys_buffer = Rs_buffer * np.sin(phis_buffer)

            #convert cm to m
            for x in range(len(Xs_buffer)):
                Xs_buffer[x] = Xs_buffer[x] * 0.01
                Ys_buffer[x] = Ys_buffer[x] * 0.01
                Zs_buffer[x] = Zs_buffer[x] * 0.01

            for k in range(point_count):
                coordinates_buffer[k][0] = Xs_buffer[k]
                coordinates_buffer[k][1] = Ys_buffer[k]
                coordinates_buffer[k][2] = Zs_buffer[k]
            for f in range(len(datas_buffer)):
                datas_buffer[f] = (datas_buffer[f] / 100.0) * 5e22

            #save element vertices
            element_buffer = []
            comsol_element_count = 0
            #.........surface elements
            for k in range(tor_cuts - 1):
                for l in range(pol_cuts-1):
                    lower_left_index = (k * pol_cuts) + l
                    lower_right_index = ((k + 1) * pol_cuts) + l
                    vertices = str(lower_left_index) + " " + str(lower_left_index + 1) + " " + str(lower_right_index) + " " + str(lower_right_index + 1)
                    vertices_scaled = str(lower_left_index + point_count) + " " + str(lower_left_index + 1 + point_count) + " " + str(lower_right_index + point_count) + " " + str(lower_right_index + 1 + point_count)
                    element_buffer.append(vertices)
                    element_buffer.append(vertices_scaled)
                    comsol_element_count += 2
            #..........vertical elements
            for k in range(tor_cuts):
                for l in range(pol_cuts - 1):
                    index = (k * pol_cuts) + l
                    vertices = str(index) + " " + str (index + 1) + " " + str(index + point_count) + " " + str(index + 1 + point_count) 
                    element_buffer.append(vertices)
                    comsol_element_count += 1
            #..........horizontal elements
            for k in range(tor_cuts - 1):
                for l in range(pol_cuts):
                    index = (k * pol_cuts) + l
                    vertices = str(index) + " " + str (index + pol_cuts) + " " + str(index + point_count) + " " + str(index + pol_cuts + point_count) 
                    element_buffer.append(vertices)
                    comsol_element_count += 1

            #scale surface from plasma core (5.5,0,0)
            scaling_factor = 1.001
            coordinates_scaled = np.zeros((point_count, 3))
            for k in range(point_count):
                coordinates_scaled[k][0] = ((coordinates_buffer[k][0] - 5.5) * scaling_factor) + 5.5
                coordinates_scaled[k][1] = coordinates_buffer[k][1] * scaling_factor
                coordinates_scaled[k][2] = coordinates_buffer[k][2] * scaling_factor

            #write target meshes in .mphtxt format
            with open("Targets/"+self.PFCnames[i]+".mphtxt", mode='w', newline='') as file:
                writer = csv.writer(file, delimiter='\t')
                #basics
                writer.writerow(["# Created by COMSOL Multiphysics."])
                writer.writerow([])
                writer.writerow(["# Major & minor version"])
                writer.writerow(["0 1"])
                writer.writerow(["1 # number of tags"])
                writer.writerow(["# Tags"])
                writer.writerow(["5 mesh1 "])
                writer.writerow(["1 # number of types"])
                writer.writerow(["# Types"])
                writer.writerow(["3 obj "])
                writer.writerow([])
                writer.writerow(["# --------- Object 0 ----------"])
                writer.writerow([])
                writer.writerow(["0 0 1"])
                writer.writerow(["4 Mesh # class"])
                writer.writerow(["4 # version"])
                writer.writerow(["3 # sdim"])
                writer.writerow([str(point_count*2) + " # number of mesh vertices"])
                writer.writerow(["0 # lowest mesh vertex index"])
                writer.writerow([])
                writer.writerow(["# Mesh vertex coordinates"])
                #Coordinates
                for row in coordinates_buffer:
                    writer.writerow(row)
                for row in coordinates_scaled:
                    writer.writerow(row)
                writer.writerow([])
                writer.writerow(["1 # number of element types"])
                writer.writerow([])
                writer.writerow(["# Type #0"])
                writer.writerow([])
                writer.writerow(["4 quad # type name"])
                writer.writerow([])
                writer.writerow([])
                writer.writerow(["4 # number of vertices per element"])
                writer.writerow([str(comsol_element_count) + " # number of elements"])
                #Elements
                writer.writerow(["# Elements"])
                for row in np.vstack(element_buffer):   
                    writer.writerow(row) 
                writer.writerow([])
                writer.writerow(["0 # number of geometric entity indices"])
                writer.writerow([])

            #write data in parameter table

            with open("Targets/"+self.PFCnames[i]+"_data.txt", 'w') as export:
                for f in range(len(datas_buffer)):
                    string = str(coordinates_buffer[f][0])+"\t"+str(coordinates_buffer[f][1])+"\t"+str(coordinates_buffer[f][2])+"\t"+str(datas_buffer[f])+"\n"
                    export.write(string)
    





