"""
Created on 17.09.2024

@author: grma
"""
import numpy as np
import csv
def kisslinger2comsol(filename):
    #read file to array "kisslinger"
    file = open(filename, "r")
    input = file.readlines()
    file.close()
    kisslinger = []
    for row in range(len(input)):
        line = (np.array(input[row].split(' ')))
        filtered_line = line[line != '']
        cleaned_line = np.char.replace(filtered_line, '\n', '')
        kisslinger.append(cleaned_line)
    #define array information
    tor_cuts = int(kisslinger[1][0])
    pol_cuts = int(kisslinger[1][1])
    phi = []
    r = []
    z = []
    #write to cyclindrical coordinates
    for k in range(int(tor_cuts)):
        row_index = (k * (pol_cuts + 1)) + 2     
        for l in range(pol_cuts):
            phi.append(float(kisslinger[row_index][0]))
            r.append(float(kisslinger[row_index + l + 1][0]))
            z.append(float(kisslinger[row_index + l + 1][1]))
    #transfer to cartesian
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    point_count = tor_cuts * pol_cuts

    #convert cm to m
    for a in range(point_count):
        x[a] = x[a] * 0.01
        y[a] = y[a] * 0.01
        z[a] = z[a] * 0.01

    element_buffer = []
    #save connections of the coordinates
    for k in range(tor_cuts - 1):
        for l in range(pol_cuts-1):
            lower_left_index = (k * pol_cuts) + l
            lower_right_index = ((k + 1) * pol_cuts) + l
            vertices = str(lower_left_index) + " " + str(lower_left_index + 1) + " " + str(lower_right_index) + " " + str(lower_right_index + 1)
            element_buffer.append(vertices)

    #write in one array for csv creation
    coordinates_buffer = np.zeros((point_count, 3))
    for i in range(point_count):
        coordinates_buffer[i][0] = x[k]
        coordinates_buffer[i][1] = y[k]
        coordinates_buffer[i][2] = z[k]

    element_count = (tor_cuts - 1) * (pol_cuts - 1)

    #write data in .mphtxt format
    with open(filename+"_comsol.mphtxt", mode='w', newline='') as file:
        writer = csv.writer(file, delimiter='\t')
        #basics
        writer.writerow(["# Created by COMSOL Multiphysics."])
        writer.writerow([])
        writer.writerow(["# Major & minor version"])
        writer.writerow(["0 1"])
        writer.writerow(["1 # number of tags"])
        writer.writerow(["# Tags"])
        writer.writerow(["5 mesh1 "])
        writer.writerow(["1 # number of types"])
        writer.writerow(["# Types"])
        writer.writerow(["3 obj "])
        writer.writerow([])
        writer.writerow(["# --------- Object 0 ----------"])
        writer.writerow([])
        writer.writerow(["0 0 1"])
        writer.writerow(["4 Mesh # class"])
        writer.writerow(["4 # version"])
        writer.writerow(["3 # sdim"])
        writer.writerow([str(point_count) + " # number of mesh vertices"])
        writer.writerow(["0 # lowest mesh vertex index"])
        writer.writerow([])
        writer.writerow(["# Mesh vertex coordinates"])
        #Coordinates
        for row in coordinates_buffer:
            writer.writerow(row)
        writer.writerow([])
        writer.writerow(["1 # number of element types"])
        writer.writerow([])
        writer.writerow(["# Type #0"])
        writer.writerow([])
        writer.writerow(["4 quad # type name"])
        writer.writerow([])
        writer.writerow([])
        writer.writerow(["4 # number of vertices per element"])
        writer.writerow([str(element_count) + " # number of elements"])
        #Elements
        writer.writerow(["# Elements"])
        for row in np.vstack(element_buffer):   
            writer.writerow(row) 
        writer.writerow([])
        writer.writerow(["0 # number of geometric entity indices"])
        writer.writerow([])
   




